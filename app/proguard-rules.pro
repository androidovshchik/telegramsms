-keepattributes SourceFile,LineNumberTable
# -renamesourcefileattribute SourceFile
-repackageclasses

# only for debug
-keepnames,allowoptimization class defpackage.telegramsms.** { *; }

-keep class org.drinkless.td.libcore.telegram.** { *; }