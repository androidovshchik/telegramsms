package defpackage.telegramsms

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import androidx.sqlite.db.SimpleSQLiteQuery
import com.chibatching.kotpref.bulk
import defpackage.telegramsms.data.Preferences
import defpackage.telegramsms.extensions.*
import defpackage.telegramsms.models.LogTree
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_prompt.*
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.powerManager
import org.jetbrains.anko.sdk19.listeners.onItemSelectedListener
import org.jetbrains.anko.toast
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : Activity() {

    private lateinit var preferences: Preferences

    private lateinit var promptDialog: AlertDialog

    private val receiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            promptDialog.apply {
                if (intent.getBooleanExtra("prompted", false)) {
                    dismiss()
                } else {
                    show()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n", "BatteryLife")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        setContentView(R.layout.activity_main)
        preferences = Preferences(applicationContext)
        promptDialog = AlertDialog.Builder(this)
            .setTitle("Код от телеграм")
            .setView(View.inflate(applicationContext, R.layout.dialog_prompt, null))
            .setPositiveButton(getString(android.R.string.ok), null)
            .setCancelable(false)
            .create()
        promptDialog.setOnShowListener {
            val button = promptDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener { view ->
                view.lock {
                    val input = promptDialog.code.text.toString()
                    if (!TextUtils.isEmpty(input)) {
                        MainService.start(applicationContext, "code" to input)
                        promptDialog.apply {
                            code.setText("")
                            dismiss()
                        }
                    }
                }
            }
        }
        preferences.apply {
            phone?.let {
                phone_number.apply {
                    setText("+$it")
                    isEnabled = false
                }
            }
            proxies.setSelection(proxyType, false)
            proxy_url.apply {
                setText(proxyUrl)
                isEnabled = proxyType > 0
            }
            switch_run.isChecked = runService
            enable_log.isChecked = enableLog
        }
        proxies.onItemSelectedListener {
            onItemSelected { _, _, position, _ ->
                preferences.proxyType = position
                proxy_url.isEnabled = position > 0
            }
        }
        switch_run.setOnCheckedChangeListener { view, isChecked ->
            view.lock {
                if (isChecked) {
                    if (!areGranted(Manifest.permission.READ_SMS)) {
                        toast("Требуется предоставить разрешения")
                        view.isChecked = false
                        return@setOnCheckedChangeListener
                    }
                    preferences.bulk {
                        if (phone == null) {
                            val number = phone_number.text.toString()
                                .replace(MainService.phoneRegex, "")
                            if (number.length != 11) {
                                toast("Заполните телефон")
                                view.isChecked = false
                                return@setOnCheckedChangeListener
                            }
                            phone_number.apply {
                                setText("+$number")
                                isEnabled = false
                            }
                            phone = number
                        }
                        if (proxyType > 0) {
                            val proxy = proxy_url.text.toString().trim()
                            if (!MainService.proxyRegex.matches(proxy)) {
                                toast("Заполните прокси")
                                view.isChecked = false
                                return@setOnCheckedChangeListener
                            }
                            proxyUrl = proxy
                        }
                        lastTime = currentTimeMillis()
                    }
                }
                if (MainService.toggle(applicationContext, isChecked)) {
                    preferences.runService = isChecked
                }
            }
        }
        export_db.setOnClickListener { view ->
            view.isEnabled = false
            val viewRef = WeakReference(view)
            doAsync {
                MainApp.db.baseDao().checkpoint(SimpleSQLiteQuery("pragma wal_checkpoint(full)"))
                copyDb(viewRef.get()?.context)
                activityUiThread {
                    toast("БД успешно экспортировано")
                    viewRef.get()?.isEnabled = true
                }
            }
        }
        enable_log.setOnCheckedChangeListener { view, isChecked ->
            view.lock {
                preferences.enableLog = isChecked
                LogTree.saveToFile = isChecked
            }
        }
        // NOTICE this violates Google Play policy
        if (isMarshmallowPlus()) {
            if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
                startActivity(
                    Intent(
                        Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                        Uri.parse("package:$packageName")
                    )
                )
            }
        }
        if (!areGranted(Manifest.permission.READ_SMS)) {
            requestPermissions(100, Manifest.permission.READ_SMS)
        }
        registerReceiver(receiver, IntentFilter("TGM_PROMPT"))
    }

    override fun onStart() {
        super.onStart()
        MainService.toggle(applicationContext, preferences.runService)
    }

    private fun copyDb(context: Context?) = context?.run {
        getExternalFilesDir(null)?.let {
            val folder = File(it, "backup").apply {
                mkdirs()
            }
            val formatter = SimpleDateFormat("dd.MM.yyyy_HH.mm.ss", Locale.US)
            val distFile = File(folder, "app_${formatter.format(currentTimeMillis(false))}.db")
            val dbFile = getDatabasePath("app.db")
            try {
                FileInputStream(dbFile).use { input ->
                    FileOutputStream(distFile).use { output ->
                        input.copyTo(output)
                    }
                }
                return true
            } catch (e: Throwable) {
                Timber.e(e)
                distFile.delete()
            }
        }
        false
    } ?: false

    override fun onDestroy() {
        promptDialog.dismiss()
        unregisterReceiver(receiver)
        super.onDestroy()
    }
}
