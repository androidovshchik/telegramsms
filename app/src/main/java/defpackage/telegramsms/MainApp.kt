package defpackage.telegramsms

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import androidx.room.Room
import androidx.room.RoomDatabase
import com.elvishew.xlog.LogConfiguration
import com.elvishew.xlog.XLog
import com.elvishew.xlog.flattener.PatternFlattener
import com.elvishew.xlog.printer.file.FilePrinter
import com.elvishew.xlog.printer.file.backup.NeverBackupStrategy
import com.elvishew.xlog.printer.file.naming.DateFileNameGenerator
import com.facebook.stetho.Stetho
import defpackage.telegramsms.data.Database
import defpackage.telegramsms.data.Preferences
import defpackage.telegramsms.extensions.isOreoPlus
import defpackage.telegramsms.models.LogTree
import org.acra.ACRA
import org.acra.config.CoreConfigurationBuilder
import org.acra.config.DialogConfigurationBuilder
import org.acra.config.MailSenderConfigurationBuilder
import org.acra.data.StringFormat
import org.jetbrains.anko.notificationManager
import timber.log.Timber
import java.io.File
import java.nio.charset.Charset
import java.nio.charset.CharsetEncoder

@Suppress("unused")
class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        db = Room.databaseBuilder(applicationContext, Database::class.java, "app.db")
            .fallbackToDestructiveMigration()
            .setJournalMode(RoomDatabase.JournalMode.WRITE_AHEAD_LOGGING)
            .build()
        getExternalFilesDir(null)?.let {
            val folder = File(it, "logs").apply {
                mkdirs()
            }
            val config = LogConfiguration.Builder()
                .t()
                .build()
            val filePrinter = FilePrinter.Builder(folder.path)
                .fileNameGenerator(DateFileNameGenerator())
                .backupStrategy(NeverBackupStrategy())
                .flattener(PatternFlattener("{d yyyy-MM-dd HH:mm:ss.SSS} {l}: {m}"))
                .build()
            XLog.init(config, filePrinter)
        }
        val preferences = Preferences(applicationContext)
        Timber.plant(LogTree(preferences.enableLog))
        ACRA.init(this, CoreConfigurationBuilder(applicationContext)
            .setBuildConfigClass(BuildConfig::class.java)
            .setReportFormat(StringFormat.KEY_VALUE_LIST)
            .setEnabled(true).apply {
                getPluginConfigurationBuilder(MailSenderConfigurationBuilder::class.java)
                    .setMailTo("")
                    .setResSubject(R.string.crash_subject)
                    .setReportFileName("logs.txt")
                    .setReportAsFile(true)
                    .setEnabled(true)
                getPluginConfigurationBuilder(DialogConfigurationBuilder::class.java)
                    .setResTheme(R.style.DialogTheme_Crash)
                    .setResTitle(R.string.crash_title)
                    .setResText(R.string.crash_text)
                    .setResCommentPrompt(R.string.crash_comment)
                    .setResEmailPrompt(R.string.crash_email)
                    .setEnabled(true)
            })
        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                Stetho.newInitializerBuilder(applicationContext)
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(applicationContext))
                    .build()
            )
        }
        if (isOreoPlus()) {
            notificationManager.createNotificationChannel(
                NotificationChannel("low", "Low", NotificationManager.IMPORTANCE_LOW)
            )
            notificationManager.createNotificationChannel(
                NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT)
            )
        }
    }

    companion object {

        lateinit var db: Database

        val ascii: CharsetEncoder = Charset.forName("US-ASCII").newEncoder()
    }
}