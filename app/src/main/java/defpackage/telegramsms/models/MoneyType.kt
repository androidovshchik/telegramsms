@file:Suppress("SpellCheckingInspection")

package defpackage.telegramsms.models

import defpackage.telegramsms.MainApp
import timber.log.Timber

/**
 * Надо будет команды в чате например приход и расход,
 * бот пишет сколько, так же как то или обнулять или закрывать день,
 * чтобы потом заново считал за след день,
 * ну и можно чтобы еще суммарные итоговые суммы вел.
 * И кол-во смс с пополнением и списанием
 */
@Suppress("unused")
enum class MoneyType(
    val id: Int,
    val enRegex: Regex,
    val ruRegex: Regex
) {
    INCOME(
        10,
        ".*(Perevod na|popolnen na|otpravil vam|Postuplenie na).*".toRegex(),
        ".*(отправил вам|Поступление на).*".toRegex()
    ),
    EXPENSE(
        20,
        "^\\s*Spisanie c.*".toRegex(),
        "^\\s*Списание [сc].*".toRegex()
    );

    companion object {

        private val map = values().associateBy(MoneyType::id)

        fun fromText(text: String?): Argument<MoneyType>? {
            if (text == null) {
                return null
            }
            val useEn = MainApp.ascii.canEncode(text)
            val mask = when {
                useEn -> "rub."
                else -> "руб."
            }
            val indexRub = text.indexOf(mask, 0, true)
            if (indexRub >= 6/* 0.00 r*/) {
                val indexSpace = text.lastIndexOf(" ", indexRub - 2)
                if (indexSpace >= 0) {
                    try {
                        val money = text.substring(indexSpace, indexRub).trim().toFloat()
                        for (type in values()) {
                            val arg = when {
                                useEn -> if (type.enRegex.matches(text)) Argument(type) else null
                                else -> if (type.ruRegex.matches(text)) Argument(type) else null
                            }
                            if (arg != null) {
                                return arg.apply {
                                    param = money
                                }
                            }
                        }
                    } catch (e: Throwable) {
                        Timber.e(e)
                    }
                }
            }
            return null
        }
    }
}