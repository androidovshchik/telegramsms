package defpackage.telegramsms.models

class None {

    override fun toString(): String {
        return "None()"
    }
}

class Argument<T>(val enum: T) {

    /**
     * Not safe type and diff value
     */
    lateinit var param: Any

    override fun toString(): String {
        return "Argument(" +
            "enum=$enum, " +
            "param=$param, " +
            ")"
    }
}