@file:Suppress("SpellCheckingInspection")

package defpackage.telegramsms.models

import timber.log.Timber
import java.util.*

@Suppress("unused")
enum class Command(
    val regex: Regex
) {
    BOTA("^\\s*bota\\s+[\\d]{2}\\s*$".toRegex(RegexOption.IGNORE_CASE)),
    BOTU("^\\s*botu(\\s+[\\d]{2})?\\s*$".toRegex(RegexOption.IGNORE_CASE)),
    BOTS("^\\s*bots\\s*$".toRegex(RegexOption.IGNORE_CASE)),
    BOTM("^\\s*botm\\s+[\\d]{1,8}\\s*$".toRegex(RegexOption.IGNORE_CASE)),
    BOTT("^\\s*bott\\s+[\\d]{1,8}\\s*$".toRegex(RegexOption.IGNORE_CASE)),
    BOTO("^\\s*boto\\s+[\\d]{1,8}\\s*$".toRegex(RegexOption.IGNORE_CASE)),
    BOTP("^\\s*botp\\s+[\\d]{4}\\s*$".toRegex(RegexOption.IGNORE_CASE)),
    BOT ("^\\s*bot\\s*$".toRegex(RegexOption.IGNORE_CASE));

    companion object {

        @Suppress("DEPRECATION")
        fun fromText(text: String?): Argument<Command>? {
            if (text == null) {
                return null
            }
            for (type in values()) {
                if (type.regex.matches(text)) {
                    val arg = Argument(type)
                    val parts = text.trim().split("\\s+".toRegex())
                    when (type) {
                        BOTA, BOTU -> {
                            try {
                                val number = parts.getOrNull(1)?.toInt()
                                if (number != null) {
                                    require(number in 11..13 || number == 99)
                                    arg.param = number
                                } else {
                                    arg.param = 0
                                }
                            } catch (e: Throwable) {
                                Timber.e(e)
                                arg.param = -1
                            }
                        }
                        BOTM, BOTT, BOTO -> {
                            try {
                                val date = parts.getOrNull(1)
                                if (date != null) {
                                    require(date.length in 1..8 && date.length != 5 && date.length != 7)
                                    if (date.length <= 4) {
                                        arg.param = Calendar.getInstance().apply {
                                            set(Calendar.HOUR_OF_DAY, 0)
                                            set(Calendar.MINUTE, 0)
                                            set(Calendar.SECOND, 0)
                                            set(Calendar.MILLISECOND, 0)
                                        }.timeInMillis - date.toInt() * 86400_000L
                                    } else {
                                        val year = date.substring(4).toInt()
                                        arg.param = Date(
                                            if (year < 100) year + 100 else year - 1900,
                                            date.substring(2, 4).toInt() - 1,
                                            date.substring(0, 2).toInt()
                                        ).time
                                    }
                                } else {
                                    arg.param = 0L
                                }
                            } catch (e: Throwable) {
                                Timber.e(e)
                                arg.param = -1L
                            }
                        }
                        BOTP -> {
                            try {
                                val number = parts[1].toInt()
                                require(number in 1000..9999)
                                arg.param = number
                            } catch (e: Throwable) {
                                Timber.e(e)
                                arg.param = -1
                            }
                        }
                        else -> {
                        }
                    }
                    return arg
                }
            }
            return null
        }
    }
}