@file:Suppress("SpellCheckingInspection")

package defpackage.telegramsms.models

import defpackage.telegramsms.MainApp

/**
QIWI 11 тип
Akkaunt: 79648968624 summa: 6100.00 RUB. Poluchatel: Perevod na QIWI Koshelek. Kod: 548868. Nikomu ego ne soobshchaite.

QIWI 12 тип
Vash QIWI Wallet popolnen na 100.00 rub.
+7923759058 otpravil vam 1800.00 rub. Poluchite perevod na qiwi.com
Postuplenie na summu 15.00 rub. Qiwi.com
Spisanie c +79969329713 na summu 100.00 rub., poluchatel platezha +79067798613
QIWI Wallet zablokirovan. Dlya razblokirovki zayavka v razdele Pomosch na qiwi.com
+79393642258 отправил вам 50.00 руб.. Получите на qiwi.com
Поступление на сумму 28.00 руб.
Списание с +79069179519 на 200.00 руб.. Получатель: +79225644368
Ваш QIWI Кошелек заблокирован. Подробнее на qiwi.com/block

QIWI 13 тип
Odnorazovyi kod dlia smeny parolia v QIWI Wallet 4793. Nikomu ne soobshchaite.
Odnorazovyi kod dlia dostupa k QIWI Wallet cherez oplata.qiwi.com - 622855. Nikomu ne soobshchaite.
Vhod cherez QIWI android. Kod: 6738. Nikomu ego ne soobshchaite.
 */
@Suppress("unused")
enum class ContentType(
    val id: Int,
    val enRegex: Regex,
    val ruRegex: Regex,
    val phone: String,
    val allowGroup: Boolean = false,
    val supportMoney: Boolean = true
) {
    QIWI1(
        11,
        "^\\s*Akkaunt:.+Poluchatel: Perevod.+".toRegex(),
        "^\\s*Akkaunt:.+Poluchatel: Perevod.+".toRegex(),
        "QIWIWallet"
    ),
    QIWI2(
        12,
        ".*(zablokirovan|popolnen na|otpravil vam|Postuplenie na|Spisanie c).*".toRegex(),
        ".*(заблокирован|отправил вам|Поступление на|Списание [сc]).*".toRegex(),
        "QIWIWallet",
        true
    ),
    QIWI3(
        13,
        ".*(smeny parolia|dostupa k|Vhod cherez).*".toRegex(),
        ".*(smeny parolia|dostupa k|Vhod cherez).*".toRegex(),
        "QIWIWallet",
        false,
        false
    ),
    DEBUG(
        99,
        "^[\\d]$".toRegex(),
        "^[\\d]$".toRegex(),
        "",
        true
    );

    companion object {

        private val map = values().associateBy(ContentType::id)

        fun fromId(value: Int) = map[value] ?: QIWI2

        fun fromText(text: String?, phone: String?): Argument<ContentType>? {
            if (text == null || phone == null) {
                return null
            }
            if (DEBUG.enRegex.matches(text)) {
                return Argument(DEBUG).apply {
                    param = Any()
                }
            }
            val useEn = MainApp.ascii.canEncode(text)
            for (type in values()) {
                if (type.phone.equals(phone, true)) {
                    val arg = when {
                        useEn -> if (type.enRegex.matches(text)) Argument(type) else null
                        else -> if (type.ruRegex.matches(text)) Argument(type) else null
                    }
                    if (arg != null) {
                        return arg.apply {
                            param = if (type.supportMoney) {
                                MoneyType.fromText(text) ?: Any()
                            } else {
                                None()
                            }
                        }
                    }
                }
            }
            return null
        }
    }
}