@file:Suppress("SpellCheckingInspection")

package defpackage.telegramsms.models

/**
Приветствую, можно будет сделать отдельного телеграм бота статистики, чтобы считал с разных каналов,
там где даже нет вашего бот-приложения, так же списание пополнение и с сортировкой по каналам и подсчет сумм со всех каналов?

У меня сейчас 2канала с вашим и 1 с чужим, стату нужно считать на каждом отдельно и чтобы еще показывал общую с 3х

Мне надо чтобы считало как сейчас+ с чужого канала куда нет пересыла смс+ общая стата по всем вместе каналам.
 */
@Suppress("unused")
enum class SourceType(
    val id: Int,
    val suffix: String
) {
    SMS(10, ""),
    SMSTGM(20, "(С)"),
    TGM(30, "(Т)");

    companion object {

        private val map = values().associateBy(SourceType::id)

        fun fromId(value: Int?) = map[value]

        fun fromText(text: String?): Argument<SourceType>? {
            if (text == null) {
                return null
            }
            val phone = "QIWIWallet"
            // ``` ```
            val indexTilde1 = text.indexOf("```")
            if (indexTilde1 >= 0) {
                val indexTilde2 = text.indexOf("```", indexTilde1 + 3)
                if (indexTilde2 > 0) {
                    val content = text.substring(indexTilde1 + 3, indexTilde2).trim()
                    return Argument(SMSTGM).apply {
                        param = ContentType.fromText(content, phone) ?: None()
                    }
                }
            } else {
                // QIWIWallet Сообщение: Дата:
                val indexPhone = text.indexOf(phone)
                if (indexPhone >= 0) {
                    val indexMessage = text.indexOf("Сообщение:", indexPhone)
                    if (indexMessage > 0) {
                        val indexDate = text.indexOf("Дата:", indexMessage)
                        if (indexDate > 0) {
                            val content = text.substring(indexMessage + 10, indexDate).trim()
                            return Argument(TGM).apply {
                                param = ContentType.fromText(content, phone) ?: None()
                            }
                        }
                    }
                }
            }
            return null
        }
    }
}