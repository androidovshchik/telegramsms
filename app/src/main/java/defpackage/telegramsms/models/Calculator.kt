package defpackage.telegramsms.models

import defpackage.telegramsms.entities.TMoney

fun Collection<Calculator>.getTotal(): Calculator {
    val calculator = Calculator("ИТОГО")
    forEach {
        calculator.apply {
            countIncome += it.countIncome
            countExpenses += it.countExpenses
            sumIncome += it.sumIncome
            sumExpenses += it.sumExpenses
        }
    }
    return calculator
}

@Suppress("MemberVisibilityCanBePrivate")
class Calculator(val name: String?) {

    var countIncome = 0

    var countExpenses = 0

    var sumIncome = 0f

    var sumExpenses = 0f

    fun addMoney(money: List<TMoney>) {
        money.forEach {
            addMoney(it)
        }
    }

    fun addMoney(money: TMoney) {
        when (money.money) {
            MoneyType.INCOME.id -> {
                countIncome++
                sumIncome += money.amount
            }
            MoneyType.EXPENSE.id -> {
                countExpenses++
                sumExpenses += money.amount
            }
        }
    }

    fun getOutput(): String {
        return """
            $name: $sumIncome($countIncome) - $sumExpenses($countExpenses) = ${sumIncome - sumExpenses}(${countIncome + countExpenses})
        """.trimIndent()
    }
}