package defpackage.telegramsms.models

import defpackage.telegramsms.extensions.sendSync
import org.drinkless.td.libcore.telegram.TdApi

private typealias Entity = TdApi.TextEntity

@Suppress("MemberVisibilityCanBePrivate")
class BotMessage(
    val chatId: Long,
    val text: String,
    val hasMarkdown: Boolean = false,
    var showLog: Boolean = false
) {

    val entities: Array<Entity>
        get() = if (hasMarkdown) {
            sendSync<TdApi.FormattedText, Array<Entity>>(
                TdApi.ParseTextEntities(text, TdApi.TextParseModeMarkdown())
            ) {
                it.entities.apply {
                    // markdown fix
                    forEach { entity ->
                        entity.length += 7
                    }
                }
            } ?: run {
                showLog = true
                arrayOf<Entity>()
            }
        } else arrayOf()
}