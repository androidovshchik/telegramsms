package defpackage.telegramsms.data

import androidx.room.*
import defpackage.telegramsms.entities.TDialog

@Dao
abstract class DialogDao {

    @Query(
        """
        SELECT * FROM dialogs 
    """
    )
    abstract fun getAll(): List<TDialog>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(entity: TDialog): Long

    @Query(
        """
        UPDATE dialogs 
        SET td_title = :title
        WHERE td_chat = :chat
    """
    )
    abstract fun update(chat: Long, title: String)

    @Transaction
    open fun upsert(entity: TDialog) {
        val id = insert(entity)
        if (id < 0L) {
            update(entity.chat, entity.title)
        }
    }
}