package defpackage.telegramsms.data

import androidx.room.Database
import androidx.room.RoomDatabase
import defpackage.telegramsms.entities.TDialog
import defpackage.telegramsms.entities.TMoney
import defpackage.telegramsms.entities.TSubscription

@Database(
    entities = [
        TSubscription::class,
        TMoney::class,
        TDialog::class
    ],
    version = 7
)
abstract class Database : RoomDatabase() {

    abstract fun baseDao(): BaseDao

    abstract fun subscriptionDao(): SubscriptionDao

    abstract fun moneyDao(): MoneyDao

    abstract fun dialogDao(): DialogDao
}