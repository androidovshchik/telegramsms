package defpackage.telegramsms.data

import android.content.Context
import com.chibatching.kotpref.KotprefModel

class Preferences(context: Context) : KotprefModel(context) {

    override val kotprefName: String = "preferences"

    var runService by booleanPref(false, "run")

    var phone by nullableStringPref(null, "phone")

    var proxyType by intPref(0, "type")

    var proxyUrl by nullableStringPref(null, "url")

    var lastTime by longPref(0L, "last")

    var enableLog by booleanPref(false, "log")
}