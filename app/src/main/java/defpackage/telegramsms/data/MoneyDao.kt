package defpackage.telegramsms.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import defpackage.telegramsms.entities.TMoney

private typealias Money = List<TMoney>
    
@Dao
interface MoneyDao {

    /*@Query(
        """
        SELECT * FROM money 
        WHERE tm_source != :nosrc AND tm_content IN (:types)
    """
    )
    fun getAll(types: List<Int>, nosrc: Int = SourceType.SMSTGM.id): Money

    @Query(
        """
        SELECT * FROM money 
        WHERE tm_source = :src AND tm_content IN (:types)
    """
    )
    fun getAll(src: Int, types: List<Int>): Money

    @Query(
        """
        SELECT * FROM money 
        WHERE tm_chat = :chat AND tm_source = :src AND tm_content IN (:types)
    """
    )
    fun getAll(chat: Long, src: Int, types: List<Int>): Money*/

    @Query(
        """
        SELECT * FROM money 
        WHERE tm_content IN (:types) AND tm_timestamp >= :start AND tm_timestamp < :end
    """
    )
    fun getDay(types: List<Int>, start: Long, end: Long = start + DAY): Money

    @Query(
        """
        SELECT * FROM money 
        WHERE tm_source = :src AND tm_content IN (:types) AND tm_timestamp >= :start AND tm_timestamp < :end
    """
    )
    fun getDay(src: Int, types: List<Int>, start: Long, end: Long = start + DAY): Money

    @Query(
        """
        SELECT * FROM money 
        WHERE tm_chat = :chat AND tm_source = :src AND tm_content IN (:types) AND tm_timestamp >= :start AND tm_timestamp < :end
    """
    )
    fun getDay(chat: Long, src: Int, types: List<Int>, start: Long, end: Long = start + DAY): Money

    @Insert
    fun insert(entity: TMoney)

    companion object {

        private const val DAY = 86400_000L
    }
}