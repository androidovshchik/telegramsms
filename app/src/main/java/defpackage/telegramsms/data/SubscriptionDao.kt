package defpackage.telegramsms.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import defpackage.telegramsms.entities.TSubscription

@Dao
abstract class SubscriptionDao {

    @Query(
        """
        SELECT * FROM subscriptions
    """
    )
    abstract fun getAll(): List<TSubscription>

    @Insert
    abstract fun insert(entity: TSubscription): Long

    @Query(
        """
        UPDATE subscriptions 
        SET ts_prompt = :prompted
        WHERE ts_chat = :chat AND ts_content = :type
    """
    )
    abstract fun update(chat: Long, type: Int, prompted: Boolean)

    open fun update(entity: TSubscription) {
        update(entity.chat, entity.content, entity.prompted)
    }

    @Query(
        """
        DELETE FROM subscriptions
        WHERE ts_chat = :chat
    """
    )
    abstract fun deleteChat(chat: Long)

    @Query(
        """
        DELETE FROM subscriptions
        WHERE ts_chat = :chat AND ts_content = :type
    """
    )
    abstract fun deleteChat(chat: Long, type: Int)
}