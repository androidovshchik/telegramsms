package defpackage.telegramsms.extensions

import org.drinkless.td.libcore.telegram.Client
import org.drinkless.td.libcore.telegram.TdApi
import timber.log.Timber

private typealias OBJ = TdApi.Object

private typealias FUN = TdApi.Function

fun sendSync(query: FUN) {
    Client.execute(query)
}

inline fun <reified T : OBJ, R> sendSync(query: FUN, block: (T) -> R?): R? {
    val obj = Client.execute(query)
    return if (obj is T) {
        block(obj)
    } else {
        Timber.e("Expected: ${T::class.java.name} Found: $obj")
        null
    }
}

fun Client.sendAsync(query: FUN) {
    send(query, null)
}

inline fun <reified T : OBJ> Client.sendAsync(query: FUN, crossinline block: (T) -> Unit) {
    send(query) {
        if (it is T) {
            block(it)
        } else {
            Timber.e("Expected: ${T::class.java.name} Found: $it")
        }
    }
}