package defpackage.telegramsms

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.provider.Settings
import android.provider.Telephony
import androidx.core.app.NotificationCompat
import androidx.core.os.ConfigurationCompat
import com.chibatching.kotpref.blockingBulk
import com.doctoror.rxcursorloader.RxCursorLoader
import defpackage.telegramsms.commands.*
import defpackage.telegramsms.data.Database
import defpackage.telegramsms.data.Preferences
import defpackage.telegramsms.entities.TDialog
import defpackage.telegramsms.entities.TMoney
import defpackage.telegramsms.entities.TSubscription
import defpackage.telegramsms.extensions.*
import defpackage.telegramsms.models.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.drinkless.td.libcore.telegram.Client
import org.drinkless.td.libcore.telegram.TdApi
import org.jetbrains.anko.*
import timber.log.Timber
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

@SuppressLint("InlinedApi")
class MainService : Service() {

    private val disposable = CompositeDisposable()

    private lateinit var preferences: Preferences

    private lateinit var wakeLock: PowerManager.WakeLock

    private var device: String? = null

    private val startSeconds = currentTimeMillis(false) / 1000

    private var isAuthorized = AtomicBoolean(false)

    lateinit var client: Client

    val subscriptions = arrayListOf<TSubscription>()

    val messages = LinkedBlockingQueue<BotMessage>()

    var lastSubscribe = 0L

    var lastPrompt = 0L

    val db: Database
        get() = MainApp.db

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @SuppressLint("WakelockTimeout")
    override fun onCreate() {
        super.onCreate()
        startForeground(
            Int.MAX_VALUE, NotificationCompat.Builder(applicationContext, "low")
                .setSmallIcon(R.drawable.ic_send_white_24dp)
                .setContentTitle("Фоновая работа с Telegram")
                .setContentText("(-, - )…zzzZZZ")
                .setContentIntent(pendingActivityFor<MainActivity>())
                .setOngoing(true)
                .setSound(null)
                .build()
        )
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, javaClass.name).apply {
            acquire()
        }
        Timber.i("Current time offset: ${offsetTimeMillis()}")
        device = Settings.Secure.getString(contentResolver, "bluetooth_name")
        preferences = Preferences(applicationContext)
        disposable.add(Single.just(true)
            .observeOn(Schedulers.io())
            .subscribe({
                synchronized(subscriptions) {
                    subscriptions.apply {
                        clear()
                        addAll(db.subscriptionDao().getAll())
                    }
                }
            }, {
                Timber.e(it)
            })
        )
        initClient()
        val smsQuery = RxCursorLoader.Query.Builder()
            .setContentUri(Uri.parse("content://sms"))
            .setSortOrder("${Telephony.Sms.DATE} DESC LIMIT 5")
            .create()
        disposable.add(RxCursorLoader.observable(contentResolver, smsQuery, Schedulers.io())
            .subscribe({ cursor ->
                cursor.use {
                    Timber.i("content://sms ${it.count}")
                    if (it.moveToLast()) {
                        do {
                            try {
                                onNextSms(it)
                            } catch (e: Throwable) {
                                Timber.e(e)
                            }
                        } while (it.moveToPrevious())
                    }
                }
            }, {
                Timber.e(it)
            }))
        disposable.add(
            Observable.interval(200, TimeUnit.MILLISECONDS, Schedulers.computation())
            .subscribe({
                if (!isAuthorized.get()) {
                    return@subscribe
                }
                messages.poll()?.let {
                    val message = TdApi.InputMessageText(
                        TdApi.FormattedText(it.text, it.entities),
                        true,
                        true
                    )
                    client.sendAsync<TdApi.Message>(
                        TdApi.SendMessage(it.chatId, 0, true, true, null, message)
                    ) { obj ->
                        if (it.showLog) {
                            Timber.i(obj.toString())
                        }
                    }
                }
            }, {
                Timber.e(it)
            })
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            if (intent.hasExtra("code")) {
                client.send(TdApi.CheckAuthenticationCode(intent.getStringExtra("code"))) {
                    val prompted = it is TdApi.Ok
                    isAuthorized.compareAndSet(false, prompted)
                    sendBroadcast(Intent("TGM_PROMPT").apply {
                        putExtra("prompted", prompted)
                    })
                    bgToast(if (prompted) "Вы успешно авторизовались" else "Неверный код")
                }
            }
        }
        return START_STICKY
    }

    private fun initClient() {
        client = Client.create({
            when (it.constructor) {
                TdApi.UpdateNewMessage.CONSTRUCTOR -> {
                    onNextMessage((it as TdApi.UpdateNewMessage).message)
                }
            }
        }, null, null)
        val tdLibParams = TdApi.TdlibParameters(
            false,
            getDatabasePath("file").parent,
            filesDir.absolutePath,
            true,
            true,
            true,
            true,
            913999,
            "ce68cff091f15a9c559c783c40a432d1",
            ConfigurationCompat.getLocales(resources.configuration).get(0).language,
            Build.DEVICE,
            Build.VERSION.RELEASE,
            BuildConfig.VERSION_NAME,
            true,
            false
        )
        sendSync(TdApi.SetLogVerbosityLevel(2))
        client.sendAsync<TdApi.Ok>(TdApi.SetTdlibParameters(tdLibParams)) { _ ->
            client.sendAsync<TdApi.Ok>(TdApi.SetDatabaseEncryptionKey("telegram_sms".toByteArray())) { _ ->
                client.sendAsync<TdApi.Proxies>(TdApi.GetProxies()) {
                    if (preferences.proxyType > 0) {
                        try {
                            val url = URL("http://${preferences.proxyUrl}")
                            val (user, password) = url.userInfo?.split(":") ?: listOf("", "")
                            val type = when (preferences.proxyType) {
                                1 -> TdApi.ProxyTypeSocks5(user, password)
                                2 -> TdApi.ProxyTypeHttp(user, password, false)
                                else -> TdApi.ProxyTypeHttp(user, password, true)
                            }
                            client.sendAsync(
                                if (it.proxies.isEmpty()) {
                                    TdApi.AddProxy(url.host, url.port, true, type)
                                } else {
                                    val proxy = it.proxies[0].id
                                    TdApi.EditProxy(proxy, url.host, url.port, true, type)
                                }
                            )
                        } catch (e: Throwable) {
                            Timber.e(e)
                        }
                    } else {
                        it.proxies.forEach { proxy ->
                            client.sendAsync(TdApi.RemoveProxy(proxy.id))
                        }
                    }
                }
                client.sendAsync<TdApi.AuthorizationState>(TdApi.GetAuthorizationState()) {
                    when (it) {
                        is TdApi.AuthorizationStateWaitPhoneNumber -> {
                            client.sendAsync(
                                TdApi.SetAuthenticationPhoneNumber(
                                    preferences.phone,
                                    TdApi.PhoneNumberAuthenticationSettings(false, false, false)
                                )
                            )
                            sendBroadcast(Intent("TGM_PROMPT").apply {
                                putExtra("prompted", false)
                            })
                        }
                        is TdApi.AuthorizationStateWaitCode -> {
                            client.sendAsync(
                                TdApi.SendPhoneNumberVerificationCode(
                                    preferences.phone,
                                    TdApi.PhoneNumberAuthenticationSettings(false, false, false)
                                )
                            )
                            sendBroadcast(Intent("TGM_PROMPT").apply {
                                putExtra("prompted", false)
                            })
                        }
                        is TdApi.AuthorizationStateReady -> {
                            isAuthorized.compareAndSet(false, true)
                        }
                        else -> {
                            // not all cases matches correct here e.g. TdApi.AuthorizationStateWaitPassword
                            Timber.w(it.toString())
                            bgToast(it.toString())
                        }
                    }
                }
            }
        }
    }

    @Throws(Throwable::class)
    private fun onNextSms(cursor: Cursor) {
        if (cursor.getInt(cursor.getColumnIndexOrThrow(Telephony.Sms.TYPE)) != Telephony.Sms.MESSAGE_TYPE_INBOX) {
            //Timber.i("Sms is outcoming")
            return
        }
        val timeOffset = offsetTimeMillis()
        val lastKnownTime = preferences.lastTime + timeOffset
        // it is with offset
        val smsTime = cursor.getLong(cursor.getColumnIndexOrThrow(Telephony.Sms.DATE))
        if (smsTime <= lastKnownTime) {
            //Timber.i("Sms time $smsTime <= $lastKnownTime")
            return
        }
        preferences.blockingBulk {
            lastTime = smsTime - timeOffset
        }
        val body = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.BODY))
            ?.replace(newLineRegex, " ")
        val phone = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.ADDRESS))
            ?.replace(phoneRegex, "")
        var simSlot = 1
        for (name in cursor.columnNames) {
            if (simRegex.matches(name)) {
                simSlot = cursor.getInt(cursor.getColumnIndexOrThrow(name))
                break
            }
        }
        Timber.i("Sms phone $phone sim(slot) $simSlot")
        ContentType.fromText(body, phone)?.let { arg1 ->
            Timber.i("ContentType.fromText: $arg1")
            val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.US)
            tryCast<Argument<MoneyType>>(arg1.param) { arg2 ->
                db.moneyDao().insert(TMoney().apply {
                    source = SourceType.SMS.id
                    content = arg1.enum.id
                    money = arg2.enum.id
                    amount = arg2.param as Float
                    sim = simSlot
                    timestamp = smsTime - timeOffset
                    offset = timeOffset
                    datetime = formatter.format(smsTime)
                })
            }
            val text = """
                ${simSlot.toEmoji()} ${formatter.format(smsTime)}
                ```
                $body
                ```
                $device
            """.trimIndent()
            synchronized(subscriptions) {
                subscriptions.forEach {
                    if (it.prompted && it.content == arg1.enum.id) {
                        messages.put(BotMessage(it.chat, text, true, true))
                        return
                    }
                }
            }
        }
    }

    private fun onNextMessage(message: TdApi.Message) {
        if (message.content !is TdApi.MessageText) {
            return
        }
        val text = (message.content as TdApi.MessageText).text.text
            .replace(newLineRegex, " ")
        SourceType.fromText(text)?.let { arg1 ->
            Timber.i("SourceType.fromText: $arg1")
            tryCast<Argument<ContentType>>(arg1.param) { arg2 ->
                tryCast<Argument<MoneyType>>(arg2.param) { arg3 ->
                    val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.US)
                    db.moneyDao().insert(TMoney().apply {
                        source = arg1.enum.id
                        content = arg2.enum.id
                        money = arg3.enum.id
                        amount = arg3.param as Float
                        chat = message.chatId
                        timestamp = message.date * 1000L
                        offset = offsetTimeMillis()
                        datetime = formatter.format(message.date * 1000L)
                    })
                    client.sendAsync<TdApi.Chat>(TdApi.GetChat(message.chatId)) {
                        db.dialogDao().upsert(TDialog().apply {
                            chat = it.id
                            title = it.title
                        })
                    }
                }
            }
            return
        }
        if (message.date < startSeconds) {
            return
        }
        Command.fromText(text)?.let {
            synchronized(subscriptions) {
                when (it.enum) {
                    Command.BOTA -> onBOTA(it, message)
                    Command.BOTU -> onBOTU(it, message)
                    Command.BOTS -> onBOTS(it, message)
                    Command.BOTM -> onBOTM(it, message)
                    Command.BOTO -> onBOTO(it, message)
                    Command.BOTT -> onBOTT(it, message)
                    Command.BOTP -> onBOTP(it, message)
                    Command.BOT -> onBOT(it, message)
                }
            }
        }
    }

    fun showBOTP(random: Int) {
        notificationManager.notify(
            random, NotificationCompat.Builder(applicationContext, "default")
                .setSmallIcon(R.drawable.ic_https_white_24dp)
                .setContentTitle("Код подтверждения")
                .setContentText("Напишите \"botp $random\"")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true)
                .build()
        )
    }

    override fun onDestroy() {
        disposable.dispose()
        client.close()
        notificationManager.cancelAll()
        wakeLock.release()
        super.onDestroy()
    }

    private fun Int.toEmoji(): String {
        return toString().replace("0", "0️⃣")
            .replace("1", "1️⃣")
            .replace("2", "2️⃣")
            .replace("3", "3️⃣")
            .replace("4", "4️⃣")
            .replace("5", "5️⃣")
            .replace("6", "6️⃣")
            .replace("7", "7️⃣")
            .replace("8", "8️⃣")
            .replace("9", "9️⃣")
    }

    companion object {

        const val MAX_SUBSCRIPTIONS = 1000

        val phoneRegex = "[+\\-() ]".toRegex()

        val proxyRegex =
            "^(?:(\\w+)(?::(\\w+))?@)?(?:\\d{1,3})(?:\\.\\d{1,3}){3}(?::(\\d{1,5}))?$".toRegex()

        private val newLineRegex = "(\r\n|\r|\n)".toRegex()

        private val simRegex = ".*(slot|sim).*".toRegex(RegexOption.IGNORE_CASE)

        fun toggle(context: Context, run: Boolean): Boolean = context.run {
            return if (run) {
                try {
                    start(applicationContext)
                } catch (e: Throwable) {
                    Timber.e(e)
                    toast(e.toString())
                    false
                }
            } else {
                stop(applicationContext)
            }
        }

        /**
         * @return true if service is running
         */
        @Throws(Throwable::class)
        fun start(context: Context, vararg params: Pair<String, Any?>): Boolean = context.run {
            return if (!activityManager.isRunning<MainService>()) {
                startForegroundService<MainService>() != null
            } else {
                startService<MainService>(*params) != null
            }
        }

        /**
         * @return true if service is stopped
         */
        @Suppress("MemberVisibilityCanBePrivate")
        fun stop(context: Context): Boolean = context.run {
            if (activityManager.isRunning<MainService>()) {
                return stopService<MainService>()
            }
            return true
        }
    }
}