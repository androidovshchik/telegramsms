package defpackage.telegramsms.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(
    tableName = "subscriptions"
)
class TSubscription {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ts_id")
    var id: Long? = null

    @ColumnInfo(name = "ts_chat")
    var chat = 0L

    @ColumnInfo(name = "ts_content")
    var content = 0

    @ColumnInfo(name = "ts_prompt")
    var prompted = true

    @Ignore
    var code = 0

    override fun toString(): String {
        return "TSubscription(" +
            "id=$id, " +
            "chat=$chat, " +
            "content=$content, " +
            "code=$code, " +
            "prompted=$prompted" +
            ")"
    }
}