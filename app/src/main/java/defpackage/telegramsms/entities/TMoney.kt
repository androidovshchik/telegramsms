@file:Suppress("SpellCheckingInspection")

package defpackage.telegramsms.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "money",
    indices = [
        Index(value = ["tm_source", "tm_content"])
    ]
)
class TMoney {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "tm_id")
    var id: Long? = null

    @ColumnInfo(name = "tm_source")
    var source = 0

    @ColumnInfo(name = "tm_content")
    var content = 0

    @ColumnInfo(name = "tm_money")
    var money = 0

    @ColumnInfo(name = "tm_amount")
    var amount = 0f

    @ColumnInfo(name = "tm_chat")
    var chat = 0L

    @ColumnInfo(name = "tm_sim")
    var sim = -1

    /**
     * Time with offset
     */
    @ColumnInfo(name = "tm_timestamp")
    var timestamp = 0L

    @ColumnInfo(name = "tm_offset")
    var offset = 0

    @ColumnInfo(name = "tm_datetime")
    lateinit var datetime: String

    override fun toString(): String {
        return "TMoney(" +
            "id=$id, " +
            "source=$source, " +
            "content=$content, " +
            "money=$money, " +
            "amount=$amount, " +
            "chat=$chat, " +
            "sim=$sim, " +
            "timestamp=$timestamp, " +
            "offset=$offset, " +
            "datetime='$datetime'" +
            ")"
    }
}