package defpackage.telegramsms.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "dialogs",
    indices = [
        Index(value = ["td_chat"], unique = true)
    ]
)
class TDialog {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "td_id")
    var id: Long? = null

    @ColumnInfo(name = "td_chat")
    var chat = 0L

    @ColumnInfo(name = "td_title")
    lateinit var title: String

    override fun toString(): String {
        return "TDialog(" +
            "id=$id, " +
            "chat=$chat, " +
            "title='$title'" +
            ")"
    }
}