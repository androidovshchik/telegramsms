package defpackage.telegramsms.commands

import android.text.TextUtils
import defpackage.telegramsms.MainService
import defpackage.telegramsms.models.*
import org.drinkless.td.libcore.telegram.TdApi
import java.text.SimpleDateFormat
import java.util.*

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOTM(arg: Argument<Command>, message: TdApi.Message) {
    val types = subscriptions.filter { it.chat == message.chatId && it.prompted }
        .map { it.content }
    if (types.isEmpty()) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Требуется иметь хотя бы одну активную подписку
                """.trimIndent()
            )
        )
        return
    }
    val source = SourceType.SMS
    val query: String
    val time = arg.param as Long
    val items = when {
        time <= 0L -> {
            messages.put(
                BotMessage(
                    message.chatId, """
                        Невалидный параметр выборки
                    """.trimIndent()
                )
            )
            return
        }
        else -> {
            val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.US)
            query = formatter.format(time)
            db.moneyDao().getDay(source.id, types, time)
        }
        /*else -> {
            query = "все время"
            db.moneyDao().getAll(source.id, types)
        }*/
    }
    val calculator = Calculator("СМС${source.suffix}").apply {
        addMoney(items)
    }
    messages.put(
        BotMessage(
            message.chatId, """
                💸 Выборка за $query
                ```
                ${calculator.getOutput()}
                ```
                По подпискам в чате: ${TextUtils.join(",", types)}
            """.trimIndent(), true
        )
    )
}