package defpackage.telegramsms.commands

import defpackage.telegramsms.MainService
import defpackage.telegramsms.extensions.currentTimeMillis
import defpackage.telegramsms.models.Argument
import defpackage.telegramsms.models.BotMessage
import defpackage.telegramsms.models.Command
import org.drinkless.td.libcore.telegram.TdApi
import org.jetbrains.anko.notificationManager

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOTP(arg: Argument<Command>, message: TdApi.Message) {
    val now = currentTimeMillis()
    if (now - lastPrompt < 15_000L) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Попробуйте снова через 15 секунд
                """.trimIndent()
            )
        )
        return
    }
    lastPrompt = now
    val code = arg.param as Int
    if (code < 0) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Невалидный код подтверждения
                """.trimIndent()
            )
        )
        return
    }
    val subscription = subscriptions.firstOrNull { it.code == code && !it.prompted }
    if (subscription != null) {
        db.subscriptionDao().update(subscription.apply {
            prompted = true
        })
        messages.put(
            BotMessage(
                message.chatId, """
                    Вы успешно подтвердили подписку
                """.trimIndent()
            )
        )
        notificationManager.cancel(code)
    } else {
        messages.put(
            BotMessage(
                message.chatId, """
                    Неверный код подтверждения
                """.trimIndent()
            )
        )
    }
}