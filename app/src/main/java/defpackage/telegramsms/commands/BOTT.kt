package defpackage.telegramsms.commands

import android.text.TextUtils
import defpackage.telegramsms.MainService
import defpackage.telegramsms.models.*
import org.drinkless.td.libcore.telegram.TdApi
import java.text.SimpleDateFormat
import java.util.*

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOTT(arg: Argument<Command>, message: TdApi.Message) {
    val subscriptions1 = subscriptions.filter { it.chat == message.chatId }
    val activeTypes = subscriptions1.filter { it.prompted }
        .map { it.content }
    val inactiveTypes = subscriptions1.filter { !it.prompted }
        .map { it.content }
    val types = activeTypes + inactiveTypes
    if (types.isEmpty()) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Требуется иметь хотя бы одну подписку
                """.trimIndent()
            )
        )
        return
    }
    val query: String
    val time = arg.param as Long
    val items = when {
        time <= 0L -> {
            messages.put(
                BotMessage(
                    message.chatId, """
                        Невалидный параметр выборки
                    """.trimIndent()
                )
            )
            return
        }
        else -> {
            val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.US)
            query = formatter.format(time)
            db.moneyDao().getDay(types, time)
        }
        /*else -> {
            query = "все время"
            db.moneyDao().getAll(types)
        }*/
    }
    val dialogs = db.dialogDao().getAll()
    val calculators = arrayListOf<Calculator>()
    /*if (activeTypes.isNotEmpty()) {
        val source = SourceType.SMS
        val calculator = Calculator("СМС${source.suffix}").apply {
            addMoney(items.filter { it.source == source.id && it.content in activeTypes })
        }
        calculators.add(calculator)
    }*/
    items.groupBy { it.chat }.forEach { entry ->
        if (entry.value.isEmpty()) {
            return@forEach
        }
        if (entry.value[0].chat == 0L) {
            return@forEach
        }
        val source: SourceType
        val subitems = if (entry.value.firstOrNull { it.source == SourceType.SMSTGM.id } != null) {
            source = SourceType.SMSTGM
            entry.value.filter { it.source == source.id }
        } else {
            source = SourceType.TGM
            entry.value.filter { it.source == source.id }
        }
        val title = dialogs.firstOrNull { it.chat == entry.key }?.title
        val calculator = Calculator("$title${source.suffix}").apply {
            addMoney(subitems)
        }
        calculators.add(calculator)
    }
    calculators.sortByDescending { it.sumIncome - it.sumExpenses }
    messages.put(
        BotMessage(
            message.chatId, """
                💸 Выборка за $query
                ```
                ${if (calculators.isNotEmpty()) {
                calculators.add(calculators.getTotal())
                TextUtils.join("\n                ", calculators.map { it.getOutput() })
            } else {
                "Нет данных"
            }}
                ```
                По подпискам в чате: ${TextUtils.join(",", types)}
            """.trimIndent(), true
        )
    )
}