package defpackage.telegramsms.commands

import defpackage.telegramsms.MainService
import defpackage.telegramsms.entities.TSubscription
import defpackage.telegramsms.extensions.currentTimeMillis
import defpackage.telegramsms.extensions.sendAsync
import defpackage.telegramsms.models.Argument
import defpackage.telegramsms.models.BotMessage
import defpackage.telegramsms.models.Command
import defpackage.telegramsms.models.ContentType
import org.drinkless.td.libcore.telegram.TdApi

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOTA(arg: Argument<Command>, message: TdApi.Message) {
    val now = currentTimeMillis()
    if (now - lastSubscribe < 3_000L) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Попробуйте снова через 3 секунд
                """.trimIndent()
            )
        )
        return
    }
    lastSubscribe = now
    if (subscriptions.size >= MainService.MAX_SUBSCRIPTIONS) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Превышен общий лимит подписок
                """.trimIndent()
            )
        )
        return
    }
    val id = arg.param as Int
    if (id <= 0) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Неизвестный тип рассылки
                """.trimIndent()
            )
        )
        return
    }
    val type = ContentType.fromId(id)
    if (type.allowGroup) {
        addSubscription(message.chatId, type.id)
    } else {
        client.sendAsync<TdApi.Chat>(TdApi.GetChat(message.chatId)) {
            when (it.type) {
                is TdApi.ChatTypeBasicGroup, is TdApi.ChatTypeSupergroup -> {
                    messages.put(
                        BotMessage(
                            message.chatId, """
                                Данный тип рассылки недоступен для группового чата
                            """.trimIndent()
                        )
                    )
                }
                else -> {
                    synchronized(subscriptions) {
                        addSubscription(message.chatId, type.id, false)
                    }
                }
            }
        }
    }
}

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.addSubscription(chatId: Long, type: Int, showError: Boolean = true) {
    var subscription = subscriptions.firstOrNull { it.chat == chatId && it.content == type }
    if (subscription?.prompted == true) {
        if (showError) {
            messages.put(
                BotMessage(
                    chatId, """
                        Такая подписка уже активирована
                    """.trimIndent()
                )
            )
        }
        return
    }
    val random = (1000..9999).random()
    if (subscription == null) {
        subscription = TSubscription().apply {
            chat = chatId
            content = type
            prompted = false
            code = random
        }
        subscriptions.add(subscription)
        db.subscriptionDao().insert(subscription)
    } else {
        subscription.code = random
        messages.put(
            BotMessage(
                chatId, """
                    Такая подписка сейчас не активна
                """.trimIndent()
            )
        )
    }
    showBOTP(random)
}