package defpackage.telegramsms.commands

import defpackage.telegramsms.MainService
import defpackage.telegramsms.R
import defpackage.telegramsms.models.Argument
import defpackage.telegramsms.models.BotMessage
import defpackage.telegramsms.models.Command
import org.drinkless.td.libcore.telegram.TdApi

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOT(arg: Argument<Command>, message: TdApi.Message) {
    val about = resources.openRawResource(R.raw.about)
        .bufferedReader().use { it.readText() }
    messages.put(BotMessage(message.chatId, about, true))
}