package defpackage.telegramsms.commands

import android.text.TextUtils
import defpackage.telegramsms.MainService
import defpackage.telegramsms.extensions.sendAsync
import defpackage.telegramsms.models.*
import org.drinkless.td.libcore.telegram.TdApi
import java.text.SimpleDateFormat
import java.util.*

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOTO(arg: Argument<Command>, message: TdApi.Message) {
    val subscriptions1 = subscriptions.filter { it.chat == message.chatId }
    if (subscriptions1.isEmpty()) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Требуется иметь хотя бы одну подписку
                """.trimIndent()
            )
        )
        return
    }
    val source: SourceType
    val types = if (subscriptions1.firstOrNull { it.prompted } != null) {
        source = SourceType.SMSTGM
        subscriptions1.filter { it.prompted }
            .map { it.content }
    } else {
        source = SourceType.TGM
        subscriptions1.filter { !it.prompted }
            .map { it.content }
    }
    val query: String
    val time = arg.param as Long
    val items = when {
        time <= 0L -> {
            messages.put(
                BotMessage(
                    message.chatId, """
                        Невалидный параметр выборки
                    """.trimIndent()
                )
            )
            return
        }
        else -> {
            val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.US)
            query = formatter.format(time)
            db.moneyDao().getDay(message.chatId, source.id, types, time)
        }
        /*else -> {
            query = "все время"
            db.moneyDao().getAll(message.chatId, source.id, types)
        }*/
    }
    client.sendAsync<TdApi.Chat>(TdApi.GetChat(message.chatId)) { chat ->
        val calculator = Calculator("${chat.title}${source.suffix}").apply {
            addMoney(items)
        }
        messages.put(
            BotMessage(
                message.chatId, """
                    💸 Выборка за $query
                    ```
                    ${calculator.getOutput()}
                    ```
                    По подпискам в чате: ${TextUtils.join(",", types)}
                """.trimIndent(), true
            )
        )
    }
}