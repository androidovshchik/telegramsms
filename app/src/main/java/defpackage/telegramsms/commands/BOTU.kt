package defpackage.telegramsms.commands

import defpackage.telegramsms.MainService
import defpackage.telegramsms.models.Argument
import defpackage.telegramsms.models.BotMessage
import defpackage.telegramsms.models.Command
import org.drinkless.td.libcore.telegram.TdApi

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOTU(arg: Argument<Command>, message: TdApi.Message) {
    val id = arg.param as Int
    if (id < 0) {
        messages.put(
            BotMessage(
                message.chatId, """
                    Неизвестный тип рассылки
                """.trimIndent()
            )
        )
        return
    }
    if (id > 0) {
        subscriptions.removeAll { it.chat == message.chatId && it.content == id }
        db.subscriptionDao().deleteChat(message.chatId, id)
        messages.put(
            BotMessage(
                message.chatId, """
                Вы успешно отписались от рассылки
            """.trimIndent()
            )
        )
    } else {
        subscriptions.removeAll { it.chat == message.chatId }
        db.subscriptionDao().deleteChat(message.chatId)
        messages.put(
            BotMessage(
                message.chatId, """
                Вы успешно отписались от всех рассылок
            """.trimIndent()
            )
        )
    }
}