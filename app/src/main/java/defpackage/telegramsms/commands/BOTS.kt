package defpackage.telegramsms.commands

import android.text.TextUtils
import defpackage.telegramsms.MainService
import defpackage.telegramsms.models.Argument
import defpackage.telegramsms.models.BotMessage
import defpackage.telegramsms.models.Command
import org.drinkless.td.libcore.telegram.TdApi

@Suppress("NOTHING_TO_INLINE")
inline fun MainService.onBOTS(arg: Argument<Command>, message: TdApi.Message) {
    val all = subscriptions.filter { it.chat == message.chatId }
    val active = all.filter { it.prompted }.map { it.content }
    val inactive = all.filter { !it.prompted }.map { it.content }
    messages.put(
        BotMessage(
            message.chatId, """
                Активные подписки в чате: ${if (active.isNotEmpty()) {
                TextUtils.join(",", active)
            } else "нет"}
                Неактивные подписки в чате: ${if (inactive.isNotEmpty()) {
                TextUtils.join(",", inactive)
            } else "нет"}
            """.trimIndent()
        )
    )
}