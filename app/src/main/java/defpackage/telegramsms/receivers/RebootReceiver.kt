package defpackage.telegramsms.receivers

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import defpackage.telegramsms.MainService
import defpackage.telegramsms.data.Preferences

class RebootReceiver : BroadcastReceiver() {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context, intent: Intent) {
        val preferences = Preferences(context)
        if (preferences.runService) {
            MainService.toggle(context, true)
        }
    }
}